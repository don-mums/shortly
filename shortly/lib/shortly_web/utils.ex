defmodule ShortlyWeb.Utils do
  def redirects_topic(host) do
    "num_redirects:#{host}"
  end
end
