defmodule ShortlyWeb.Admin.VisitView do
  use ShortlyWeb, :view

  import Torch.TableView
  import Torch.FilterView
end
