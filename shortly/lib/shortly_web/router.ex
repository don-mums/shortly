defmodule ShortlyWeb.Router do
  use ShortlyWeb, :router
  use Pow.Phoenix.Router

  use Plug.ErrorHandler

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {ShortlyWeb.LayoutView, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :protected do
    plug Pow.Plug.RequireAuthenticated,
      error_handler: Pow.Phoenix.PlugErrorHandler
  end

  scope "/" do
    pipe_through [:browser, :get_remote_ip]

    live "/", ShortlyWeb.PageLive, :index, layout: "homepage.html"
    get "/:slug", ShortlyWeb.PageController, :page_redirect

    pow_session_routes()
  end

  scope "/registration", Pow.Phoenix, as: "pow" do
    pipe_through [:browser, :protected]

    resources "/", RegistrationController, singleton: true, only: [:edit, :update, :delete]
  end

  scope "/admin", ShortlyWeb.Admin, as: :admin do
    pipe_through [:browser, :protected]

    resources "/domains", DomainController
    resources "/urls", UrlController
    resources "/visits", VisitController
  end

  # Other scopes may use custom stacks.
  # scope "/api", ShortlyWeb do
  #   pipe_through :api
  # end

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).

  import Phoenix.LiveDashboard.Router

  scope "/admin" do
    pipe_through [:browser, :protected]

    live_dashboard "/dashboard",
      metrics: ShortlyWeb.Telemetry,
      metrics_history: {ShortlyWeb.TelemetryStorage, :metrics_history, []},
      ecto_repos: [Shortly.Repo]
  end

  defp get_remote_ip(conn, _) do
    put_session(conn, :remote_ip, conn.remote_ip)
  end
end
