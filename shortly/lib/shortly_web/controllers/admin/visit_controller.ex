defmodule ShortlyWeb.Admin.VisitController do
  use ShortlyWeb, :controller

  alias Shortly.Chibi
  alias Shortly.Chibi.Visit

  plug(:put_layout, {ShortlyWeb.LayoutView, "torch.html"})

  def index(conn, params) do
    case Chibi.paginate_visits(params) do
      {:ok, assigns} ->
        render(conn, "index.html", assigns)

      error ->
        conn
        |> put_flash(:error, "There was an error rendering Visits. #{inspect(error)}")
        |> redirect(to: Routes.admin_visit_path(conn, :index))
    end
  end

  def new(conn, _params) do
    changeset = Chibi.change_visit(%Visit{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"visit" => visit_params}) do
    case Chibi.create_visit(visit_params) do
      {:ok, visit} ->
        conn
        |> put_flash(:info, "Visit created successfully.")
        |> redirect(to: Routes.admin_visit_path(conn, :show, visit))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    visit = Chibi.get_visit!(id)
    render(conn, "show.html", visit: visit)
  end

  def edit(conn, %{"id" => id}) do
    visit = Chibi.get_visit!(id)
    changeset = Chibi.change_visit(visit)
    render(conn, "edit.html", visit: visit, changeset: changeset)
  end

  def update(conn, %{"id" => id, "visit" => visit_params}) do
    visit = Chibi.get_visit!(id)

    case Chibi.update_visit(visit, visit_params) do
      {:ok, visit} ->
        conn
        |> put_flash(:info, "Visit updated successfully.")
        |> redirect(to: Routes.admin_visit_path(conn, :show, visit))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", visit: visit, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    visit = Chibi.get_visit!(id)
    {:ok, _visit} = Chibi.delete_visit(visit)

    conn
    |> put_flash(:info, "Visit deleted successfully.")
    |> redirect(to: Routes.admin_visit_path(conn, :index))
  end
end
