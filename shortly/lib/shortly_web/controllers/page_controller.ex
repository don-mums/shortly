defmodule ShortlyWeb.PageController do
  use ShortlyWeb, :controller

  alias Shortly.Chibi
  alias Phoenix.PubSub

  def page_redirect(conn, %{"slug" => slug}) do
    site_id = Shortly.Site.get_by_host!(conn.host).id

    case Chibi.get_by_slug!(slug, site_id) do
      nil ->
        conn
        |> send_resp(404, "Not found")
        |> halt()

      url ->
        user_agent = hd(Plug.Conn.get_req_header(conn, "user-agent"))

        referer =
          case List.keyfind(conn.req_headers, "referer", 0) do
            {"referer", referer} ->
              referer

            nil ->
              ""
          end

        {:ok, _} =
          Chibi.create_visit(url, %{
            referrer: referer,
            user_agent: user_agent
          })

        num_redirects = Chibi.get_num_redirects(site_id)

        PubSub.broadcast(
          Shortly.PubSub,
          ShortlyWeb.Utils.redirects_topic(conn.host),
          %{
            num_redirects: num_redirects
          }
        )

        redirect(conn, external: url.url)
    end
  end
end
