defmodule Shortly.Site.Domain do
  use Ecto.Schema
  import Ecto.Changeset

  schema "domains" do
    field :host, :string
    field :url, :string
    field :title, :string
    field :template, :string
    field :favicon_path, :string
    field :is_public, :boolean, default: false

    timestamps()
  end

  @doc false
  def changeset(domain, attrs) do
    domain
    |> cast(attrs, [:host, :url, :title, :template, :favicon_path, :is_public])
    |> validate_required([:host, :url, :title, :template, :favicon_path, :is_public])
  end
end
