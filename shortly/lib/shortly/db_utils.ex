defmodule Shortly.DbUtils do
  @moduledoc false
  import Ecto.Query, warn: false

  alias Shortly.Repo

  defp put_filter({field, nil}, query) do
    from(q in query, where: is_nil(field(q, ^field)))
  end

  defp put_filter({field, value}, query) do
    from(q in query, where: field(q, ^field) == ^value)
  end

  def filter_db(query, filter_params) do
    Map.to_list(filter_params)
    |> Enum.reduce(query, &put_filter/2)
  end

  def update_db(query, filter_params, attrs) do
    attrs_list = Map.to_list(attrs)

    filter_db(query, filter_params)
    |> Repo.update_all(set: attrs_list)
  end
end
