defmodule ShortlyWeb.Site.DomainLiveTest do
  use ShortlyWeb.ConnCase

  import Phoenix.LiveViewTest

  alias Shortly.Site

  @create_attrs %{domain: "some domain", is_public: true}
  @update_attrs %{domain: "some updated domain", is_public: false}
  @invalid_attrs %{domain: nil, is_public: nil}

  defp fixture(:domain) do
    {:ok, domain} = Site.create_domain(@create_attrs)
    domain
  end

  defp create_domain(_) do
    domain = fixture(:domain)
    %{domain: domain}
  end

  describe "Index" do
    setup [:create_domain]

    test "lists all domains", %{conn: conn, domain: domain} do
      {:ok, _index_live, html} = live(conn, Routes.site_domain_index_path(conn, :index))

      assert html =~ "Listing Domains"
      assert html =~ domain.domain
    end

    test "saves new domain", %{conn: conn} do
      {:ok, index_live, _html} = live(conn, Routes.site_domain_index_path(conn, :index))

      assert index_live |> element("a", "New Domain") |> render_click() =~
               "New Domain"

      assert_patch(index_live, Routes.site_domain_index_path(conn, :new))

      assert index_live
             |> form("#domain-form", domain: @invalid_attrs)
             |> render_change() =~ "can&apos;t be blank"

      {:ok, _, html} =
        index_live
        |> form("#domain-form", domain: @create_attrs)
        |> render_submit()
        |> follow_redirect(conn, Routes.site_domain_index_path(conn, :index))

      assert html =~ "Domain created successfully"
      assert html =~ "some domain"
    end

    test "updates domain in listing", %{conn: conn, domain: domain} do
      {:ok, index_live, _html} = live(conn, Routes.site_domain_index_path(conn, :index))

      assert index_live |> element("#domain-#{domain.id} a", "Edit") |> render_click() =~
               "Edit Domain"

      assert_patch(index_live, Routes.site_domain_index_path(conn, :edit, domain))

      assert index_live
             |> form("#domain-form", domain: @invalid_attrs)
             |> render_change() =~ "can&apos;t be blank"

      {:ok, _, html} =
        index_live
        |> form("#domain-form", domain: @update_attrs)
        |> render_submit()
        |> follow_redirect(conn, Routes.site_domain_index_path(conn, :index))

      assert html =~ "Domain updated successfully"
      assert html =~ "some updated domain"
    end

    test "deletes domain in listing", %{conn: conn, domain: domain} do
      {:ok, index_live, _html} = live(conn, Routes.site_domain_index_path(conn, :index))

      assert index_live |> element("#domain-#{domain.id} a", "Delete") |> render_click()
      refute has_element?(index_live, "#domain-#{domain.id}")
    end
  end

  describe "Show" do
    setup [:create_domain]

    test "displays domain", %{conn: conn, domain: domain} do
      {:ok, _show_live, html} = live(conn, Routes.site_domain_show_path(conn, :show, domain))

      assert html =~ "Show Domain"
      assert html =~ domain.domain
    end

    test "updates domain within modal", %{conn: conn, domain: domain} do
      {:ok, show_live, _html} = live(conn, Routes.site_domain_show_path(conn, :show, domain))

      assert show_live |> element("a", "Edit") |> render_click() =~
               "Edit Domain"

      assert_patch(show_live, Routes.site_domain_show_path(conn, :edit, domain))

      assert show_live
             |> form("#domain-form", domain: @invalid_attrs)
             |> render_change() =~ "can&apos;t be blank"

      {:ok, _, html} =
        show_live
        |> form("#domain-form", domain: @update_attrs)
        |> render_submit()
        |> follow_redirect(conn, Routes.site_domain_show_path(conn, :show, domain))

      assert html =~ "Domain updated successfully"
      assert html =~ "some updated domain"
    end
  end
end
