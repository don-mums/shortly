defmodule ShortlyWeb.Chibi.VisitLiveTest do
  use ShortlyWeb.ConnCase

  import Phoenix.LiveViewTest

  alias Shortly.Chibi

  @create_attrs %{referrer: "some referrer", user_agent: "some user_agent"}
  @update_attrs %{referrer: "some updated referrer", user_agent: "some updated user_agent"}
  @invalid_attrs %{referrer: nil, user_agent: nil}

  defp fixture(:visit) do
    {:ok, visit} = Chibi.create_visit(@create_attrs)
    visit
  end

  defp create_visit(_) do
    visit = fixture(:visit)
    %{visit: visit}
  end

  describe "Index" do
    setup [:create_visit]

    test "lists all visits", %{conn: conn, visit: visit} do
      {:ok, _index_live, html} = live(conn, Routes.chibi_visit_index_path(conn, :index))

      assert html =~ "Listing Visits"
      assert html =~ visit.referrer
    end

    test "saves new visit", %{conn: conn} do
      {:ok, index_live, _html} = live(conn, Routes.chibi_visit_index_path(conn, :index))

      assert index_live |> element("a", "New Visit") |> render_click() =~
               "New Visit"

      assert_patch(index_live, Routes.chibi_visit_index_path(conn, :new))

      assert index_live
             |> form("#visit-form", visit: @invalid_attrs)
             |> render_change() =~ "can&apos;t be blank"

      {:ok, _, html} =
        index_live
        |> form("#visit-form", visit: @create_attrs)
        |> render_submit()
        |> follow_redirect(conn, Routes.chibi_visit_index_path(conn, :index))

      assert html =~ "Visit created successfully"
      assert html =~ "some referrer"
    end

    test "updates visit in listing", %{conn: conn, visit: visit} do
      {:ok, index_live, _html} = live(conn, Routes.chibi_visit_index_path(conn, :index))

      assert index_live |> element("#visit-#{visit.id} a", "Edit") |> render_click() =~
               "Edit Visit"

      assert_patch(index_live, Routes.chibi_visit_index_path(conn, :edit, visit))

      assert index_live
             |> form("#visit-form", visit: @invalid_attrs)
             |> render_change() =~ "can&apos;t be blank"

      {:ok, _, html} =
        index_live
        |> form("#visit-form", visit: @update_attrs)
        |> render_submit()
        |> follow_redirect(conn, Routes.chibi_visit_index_path(conn, :index))

      assert html =~ "Visit updated successfully"
      assert html =~ "some updated referrer"
    end

    test "deletes visit in listing", %{conn: conn, visit: visit} do
      {:ok, index_live, _html} = live(conn, Routes.chibi_visit_index_path(conn, :index))

      assert index_live |> element("#visit-#{visit.id} a", "Delete") |> render_click()
      refute has_element?(index_live, "#visit-#{visit.id}")
    end
  end

  describe "Show" do
    setup [:create_visit]

    test "displays visit", %{conn: conn, visit: visit} do
      {:ok, _show_live, html} = live(conn, Routes.chibi_visit_show_path(conn, :show, visit))

      assert html =~ "Show Visit"
      assert html =~ visit.referrer
    end

    test "updates visit within modal", %{conn: conn, visit: visit} do
      {:ok, show_live, _html} = live(conn, Routes.chibi_visit_show_path(conn, :show, visit))

      assert show_live |> element("a", "Edit") |> render_click() =~
               "Edit Visit"

      assert_patch(show_live, Routes.chibi_visit_show_path(conn, :edit, visit))

      assert show_live
             |> form("#visit-form", visit: @invalid_attrs)
             |> render_change() =~ "can&apos;t be blank"

      {:ok, _, html} =
        show_live
        |> form("#visit-form", visit: @update_attrs)
        |> render_submit()
        |> follow_redirect(conn, Routes.chibi_visit_show_path(conn, :show, visit))

      assert html =~ "Visit updated successfully"
      assert html =~ "some updated referrer"
    end
  end
end
