defmodule ShortlyWeb.Chibi.UrlLiveTest do
  use ShortlyWeb.ConnCase

  import Phoenix.LiveViewTest

  alias Shortly.Chibi

  @create_attrs %{slug: "some slug", slug_id: 42, urls: "some urls"}
  @update_attrs %{slug: "some updated slug", slug_id: 43, urls: "some updated urls"}
  @invalid_attrs %{slug: nil, slug_id: nil, urls: nil}

  defp fixture(:url) do
    {:ok, url} = Chibi.create_url(@create_attrs)
    url
  end

  defp create_url(_) do
    url = fixture(:url)
    %{url: url}
  end

  describe "Index" do
    setup [:create_url]

    test "lists all urls", %{conn: conn, url: url} do
      {:ok, _index_live, html} = live(conn, Routes.chibi_url_index_path(conn, :index))

      assert html =~ "Listing Urls"
      assert html =~ url.slug
    end

    test "saves new url", %{conn: conn} do
      {:ok, index_live, _html} = live(conn, Routes.chibi_url_index_path(conn, :index))

      assert index_live |> element("a", "New Url") |> render_click() =~
               "New Url"

      assert_patch(index_live, Routes.chibi_url_index_path(conn, :new))

      assert index_live
             |> form("#url-form", url: @invalid_attrs)
             |> render_change() =~ "can&apos;t be blank"

      {:ok, _, html} =
        index_live
        |> form("#url-form", url: @create_attrs)
        |> render_submit()
        |> follow_redirect(conn, Routes.chibi_url_index_path(conn, :index))

      assert html =~ "Url created successfully"
      assert html =~ "some slug"
    end

    test "updates url in listing", %{conn: conn, url: url} do
      {:ok, index_live, _html} = live(conn, Routes.chibi_url_index_path(conn, :index))

      assert index_live |> element("#url-#{url.id} a", "Edit") |> render_click() =~
               "Edit Url"

      assert_patch(index_live, Routes.chibi_url_index_path(conn, :edit, url))

      assert index_live
             |> form("#url-form", url: @invalid_attrs)
             |> render_change() =~ "can&apos;t be blank"

      {:ok, _, html} =
        index_live
        |> form("#url-form", url: @update_attrs)
        |> render_submit()
        |> follow_redirect(conn, Routes.chibi_url_index_path(conn, :index))

      assert html =~ "Url updated successfully"
      assert html =~ "some updated slug"
    end

    test "deletes url in listing", %{conn: conn, url: url} do
      {:ok, index_live, _html} = live(conn, Routes.chibi_url_index_path(conn, :index))

      assert index_live |> element("#url-#{url.id} a", "Delete") |> render_click()
      refute has_element?(index_live, "#url-#{url.id}")
    end
  end

  describe "Show" do
    setup [:create_url]

    test "displays url", %{conn: conn, url: url} do
      {:ok, _show_live, html} = live(conn, Routes.chibi_url_show_path(conn, :show, url))

      assert html =~ "Show Url"
      assert html =~ url.slug
    end

    test "updates url within modal", %{conn: conn, url: url} do
      {:ok, show_live, _html} = live(conn, Routes.chibi_url_show_path(conn, :show, url))

      assert show_live |> element("a", "Edit") |> render_click() =~
               "Edit Url"

      assert_patch(show_live, Routes.chibi_url_show_path(conn, :edit, url))

      assert show_live
             |> form("#url-form", url: @invalid_attrs)
             |> render_change() =~ "can&apos;t be blank"

      {:ok, _, html} =
        show_live
        |> form("#url-form", url: @update_attrs)
        |> render_submit()
        |> follow_redirect(conn, Routes.chibi_url_show_path(conn, :show, url))

      assert html =~ "Url updated successfully"
      assert html =~ "some updated slug"
    end
  end
end
