defmodule ShortlyWeb.Admin.VisitControllerTest do
  use ShortlyWeb.ConnCase

  alias Shortly.Chibi

  @create_attrs %{
    referrer: "some referrer",
    site_id: 42,
    url_id: 42,
    user_agent: "some user_agent"
  }
  @update_attrs %{
    referrer: "some updated referrer",
    site_id: 43,
    url_id: 43,
    user_agent: "some updated user_agent"
  }
  @invalid_attrs %{referrer: nil, site_id: nil, url_id: nil, user_agent: nil}

  def fixture(:visit) do
    {:ok, visit} = Chibi.create_visit(@create_attrs)
    visit
  end

  describe "index" do
    test "lists all visits", %{conn: conn} do
      conn = get(conn, Routes.admin_visit_path(conn, :index))
      assert html_response(conn, 200) =~ "Visits"
    end
  end

  describe "new visit" do
    test "renders form", %{conn: conn} do
      conn = get(conn, Routes.admin_visit_path(conn, :new))
      assert html_response(conn, 200) =~ "New Visit"
    end
  end

  describe "create visit" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post conn, Routes.admin_visit_path(conn, :create), visit: @create_attrs

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == Routes.admin_visit_path(conn, :show, id)

      conn = get(conn, Routes.admin_visit_path(conn, :show, id))
      assert html_response(conn, 200) =~ "Visit Details"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post conn, Routes.admin_visit_path(conn, :create), visit: @invalid_attrs
      assert html_response(conn, 200) =~ "New Visit"
    end
  end

  describe "edit visit" do
    setup [:create_visit]

    test "renders form for editing chosen visit", %{conn: conn, visit: visit} do
      conn = get(conn, Routes.admin_visit_path(conn, :edit, visit))
      assert html_response(conn, 200) =~ "Edit Visit"
    end
  end

  describe "update visit" do
    setup [:create_visit]

    test "redirects when data is valid", %{conn: conn, visit: visit} do
      conn = put conn, Routes.admin_visit_path(conn, :update, visit), visit: @update_attrs
      assert redirected_to(conn) == Routes.admin_visit_path(conn, :show, visit)

      conn = get(conn, Routes.admin_visit_path(conn, :show, visit))
      assert html_response(conn, 200) =~ "some updated referrer"
    end

    test "renders errors when data is invalid", %{conn: conn, visit: visit} do
      conn = put conn, Routes.admin_visit_path(conn, :update, visit), visit: @invalid_attrs
      assert html_response(conn, 200) =~ "Edit Visit"
    end
  end

  describe "delete visit" do
    setup [:create_visit]

    test "deletes chosen visit", %{conn: conn, visit: visit} do
      conn = delete(conn, Routes.admin_visit_path(conn, :delete, visit))
      assert redirected_to(conn) == Routes.admin_visit_path(conn, :index)

      assert_error_sent 404, fn ->
        get(conn, Routes.admin_visit_path(conn, :show, visit))
      end
    end
  end

  defp create_visit(_) do
    visit = fixture(:visit)
    {:ok, visit: visit}
  end
end
