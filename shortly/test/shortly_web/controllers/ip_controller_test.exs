defmodule ShortlyWeb.IpControllerTest do
  use ShortlyWeb.ConnCase

  alias Shortly.Site

  @create_attrs %{ip_address: "some ip_address", is_active: true}
  @update_attrs %{ip_address: "some updated ip_address", is_active: false}
  @invalid_attrs %{ip_address: nil, is_active: nil}

  def fixture(:ip) do
    {:ok, ip} = Site.create_ip(@create_attrs)
    ip
  end

  describe "index" do
    test "lists all ips", %{conn: conn} do
      conn = get(conn, Routes.ip_path(conn, :index))
      assert html_response(conn, 200) =~ "Ips"
    end
  end

  describe "new ip" do
    test "renders form", %{conn: conn} do
      conn = get(conn, Routes.ip_path(conn, :new))
      assert html_response(conn, 200) =~ "New Ip"
    end
  end

  describe "create ip" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post conn, Routes.ip_path(conn, :create), ip: @create_attrs

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == Routes.ip_path(conn, :show, id)

      conn = get(conn, Routes.ip_path(conn, :show, id))
      assert html_response(conn, 200) =~ "Ip Details"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post conn, Routes.ip_path(conn, :create), ip: @invalid_attrs
      assert html_response(conn, 200) =~ "New Ip"
    end
  end

  describe "edit ip" do
    setup [:create_ip]

    test "renders form for editing chosen ip", %{conn: conn, ip: ip} do
      conn = get(conn, Routes.ip_path(conn, :edit, ip))
      assert html_response(conn, 200) =~ "Edit Ip"
    end
  end

  describe "update ip" do
    setup [:create_ip]

    test "redirects when data is valid", %{conn: conn, ip: ip} do
      conn = put conn, Routes.ip_path(conn, :update, ip), ip: @update_attrs
      assert redirected_to(conn) == Routes.ip_path(conn, :show, ip)

      conn = get(conn, Routes.ip_path(conn, :show, ip))
      assert html_response(conn, 200) =~ "some updated ip_address"
    end

    test "renders errors when data is invalid", %{conn: conn, ip: ip} do
      conn = put conn, Routes.ip_path(conn, :update, ip), ip: @invalid_attrs
      assert html_response(conn, 200) =~ "Edit Ip"
    end
  end

  describe "delete ip" do
    setup [:create_ip]

    test "deletes chosen ip", %{conn: conn, ip: ip} do
      conn = delete(conn, Routes.ip_path(conn, :delete, ip))
      assert redirected_to(conn) == Routes.ip_path(conn, :index)

      assert_error_sent 404, fn ->
        get(conn, Routes.ip_path(conn, :show, ip))
      end
    end
  end

  defp create_ip(_) do
    ip = fixture(:ip)
    {:ok, ip: ip}
  end
end
