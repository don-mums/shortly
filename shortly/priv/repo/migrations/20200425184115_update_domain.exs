defmodule Shortly.Repo.Migrations.UpdateDomain do
  use Ecto.Migration

  def change do
    alter table(:domains) do
      remove :domain
      add :host, :string
      add :url, :string
      add :title, :string
      add :template, :string
      add :favicon_path, :string
    end

    create unique_index(:domains, :host)
  end
end
