defmodule Shortly.Repo.Migrations.CreateVisits do
  use Ecto.Migration

  def change do
    create table(:visits) do
      add :referrer, :text
      add :user_agent, :text
      add :url_id, references(:urls, on_delete: :nothing)
      add :site_id, references(:domains, on_delete: :nothing), null: false

      timestamps()
    end

    create index(:visits, [:url_id])
    create index(:visits, [:site_id])
  end
end
