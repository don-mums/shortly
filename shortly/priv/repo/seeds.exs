# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Shortly.Repo.insert!(%Shortly.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

if Application.get_env(:shortly, Shortly)[:env] != :test do
  Shortly.Repo.insert!(%Shortly.Site.Domain{
    host: "localhost",
    url: "localhost:4000",
    title: "Localhost",
    template: "skra_cz",
    favicon_path: "/images/skra-logo.png",
    is_public: true
  })

  Shortly.Repo.insert!(%Shortly.Users.User{
    email: "mums@mums.cz",
    password_hash:
      "$pbkdf2-sha512$100000$OjoZ5ZB/AMtEbQEhe282TQ==$bhz3mWiD+cmf5Y2FwFXrqsFNf+QITMax6LN53KyrOZ8kAW4SZMRnG0T5ohOiJNXH0OIL6xrkIfyPQqwCI9QgLQ=="
  })
end
