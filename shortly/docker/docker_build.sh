#!/bin/bash

set -e
export MIX_ENV=prod

export APP_NAME=shortly
export APP_VSN=$VERSION

MIX_ENV=prod mix deps.get --only prod
MIX_ENV=prod mix do clean, compile --force --env=prod

(
    cd assets || exit
    echo "Install npm packages"
    npm install
    npm rebuild node-sass
    echo "Installation finished"
)

npm run deploy --prefix assets
MIX_ENV=prod mix phx.digest
MIX_ENV=prod mix distillery.release --env=prod

cp "_build/prod/rel/$APP_NAME/releases/$APP_VSN/$APP_NAME.tar.gz" rel/artifacts/"$APP_NAME-$APP_VSN.tar.gz"

rm -r _build/app
mkdir -p  _build/app/mnesia
tar xfz rel/artifacts/"$APP_NAME-$APP_VSN.tar.gz" -C _build/app

rm -r _build/prod
