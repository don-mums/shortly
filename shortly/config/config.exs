# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :shortly,
  ecto_repos: [Shortly.Repo]

# Configures the endpoint
config :shortly, ShortlyWeb.Endpoint,
  url: [host: "localhost"],
  render_errors: [view: ShortlyWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: Shortly.PubSub,
  live_view: [signing_salt: "ErKL718N"]

# Configures Elixir's Logger
config :logger,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id],
  backends: [:console, Sentry.LoggerBackend]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :shortly, :pow,
  user: Shortly.Users.User,
  repo: Shortly.Repo,
  web_module: ShortlyWeb,
  extensions: [PowPersistentSession],
  cache_store_backend: Pow.Store.Backend.MnesiaCache

config :torch,
  otp_app: :my_app_name,
  template_format: "eex" || "slim"

config :hammer,
  backend: {Hammer.Backend.ETS, [expiry_ms: 60_000 * 60 * 6, cleanup_interval_ms: 60_000 * 10]}

config :sentry,
  dsn: "https://e437bbbfd437433b82ace3f658009fc4@o251835.ingest.sentry.io/1820912",
  environment_name: Mix.env(),
  included_environments: [:prod],
  enable_source_code_context: true,
  root_source_code_path: File.cwd!(),
  release: "shortly@" <> to_string(Application.spec(:shortly, :vsn))

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
